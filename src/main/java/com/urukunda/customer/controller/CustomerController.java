package com.urukunda.customer.controller;

import com.urukunda.customer.model.Customer;
import com.urukunda.customer.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController (CustomerService customerService){
        this.customerService = customerService;
    }

    @PostMapping("customers")
    public Customer saveCustomer(@RequestBody Customer customer){
        return customerService.saveCustomer(customer);
    }

    @GetMapping("customers/{customerId}")
    public Customer findCustomerById(@PathVariable String customerId){
        return customerService.findCustomerById(customerId);
    }
}
