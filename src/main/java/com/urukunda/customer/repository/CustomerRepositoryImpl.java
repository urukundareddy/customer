package com.urukunda.customer.repository;

import com.urukunda.customer.model.Customer;

public class CustomerRepositoryImpl implements CustomerRepository {

    private final CustomerRepository customerRepository;

    public CustomerRepositoryImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }


    @Override
    public Customer findById(String customerId) {
        return customerRepository.findById(customerId);
    }


}
