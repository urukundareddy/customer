package com.urukunda.customer.repository;

import com.urukunda.customer.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository {

    Customer findById(String customerId);
    Customer save(Customer customer);

}
