package com.urukunda.customer.service;

import com.urukunda.customer.model.Customer;
import com.urukunda.customer.repository.CustomerRepositoryImpl;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

   private  CustomerRepositoryImpl customerRepository;

    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer findCustomerById(String customerId) {
        return customerRepository.findById(customerId);
    }
}
